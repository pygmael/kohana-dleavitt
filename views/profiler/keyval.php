<div class="kohana">
	<table class="profiler">
		<tr class="group">
			<th class="name" colspan="2"><?php echo $name ?></th>
		</tr>
		<?php foreach ($data as $key => $value): ?>
		<tr>
			<th><?php echo $key ?></th>
			<td><?php echo $value ?></td>
		</tr>
		<?php endforeach ?>
	</table>
</div>