<?php defined('SYSPATH') or die('No direct script access.');

class Pygmael_Message {
	
	public $class;
	public $content;
	
	protected static $_cleared = FALSE;
	protected static $_messages = array();
	
	public function __construct($content, $class = 'msg')
	{
		$this->content = $content;
		$this->class = $class;
	}
	
	// have to get old messages before adding new ones
	public static function get()
	{
		if ( ! Message::$_messages && Message::$_messages = Session::instance()->get('_messages_'))
		{
			Session::instance()->delete('_messages_');
			
		}
		return Message::$_messages;
	}
	
	public static function add($content, $class = 'msg')
	{
		$message = new Message($content, $class);
		$messages = Session::instance()->get('_messages_', array());
		array_push($messages, $message);
		
		Session::instance()->set('_messages_', $messages);
	}
	
}