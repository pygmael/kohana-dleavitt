<?php defined('SYSPATH') or die('No direct script access.');

class Pygmael_Heading {

	public static $headings = array();
	
	public static function generate_multiple($headings)
	{
		foreach ($headings as $label => $order_by)
		{
			Pygmael_Heading::$headings[] = Pygmael_Heading::generate($label, $order_by);
		}
		return Pygmael_Heading::$headings;
	}
	
	public static function generate($label, $order_by)
	{
		if ($order_by)
		{
			$class = 'sortable';
			$sort_direction = 'asc';
			if (Arr::get($_GET, 'order_by') == $order_by)
			{	
				if (strtolower(Arr::get($_GET, 'sort_direction', '')) === 'asc')
				{
					$class .= ' asc';
					$sort_direction = 'desc';
				}
				else
				{
					$class .= ' desc';
				}
			}
			
			return html::anchor(
				URL::site(Request::instance()->uri).URL::query(array('order_by' => $order_by, 'sort_direction' => $sort_direction)),
				Inflector::humanize($label),
				array('class' => $class)
			);
		}
		else
		{
			return Inflector::humanize($label);
		}
	}
	
}