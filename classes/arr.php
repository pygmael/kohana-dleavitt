<?php defined('SYSPATH') OR die('No direct access allowed.');

class Arr extends Kohana_Arr {

	public static function flatten($array, $return = array()) 
	{ 
		foreach($array as $x => $val) 
		{ 
			if (is_array($array[$x])) 
			{ 
				$return = Arr::flatten($array[$x],$return);
			} 
			else 
			{ 
				if ($array[$x]) 
				{ 
					$return[] = $array[$x];
				} 
			} 
		} 
		return $return;
	}
	
}