<?php defined('SYSPATH') OR die('No direct access allowed.');

class TwitterHelper {
	
	public static $user_agent = 'KohanaTwitter';
	
	public static function api($method, $params = array())
	{
	    $query = '?'.http_build_query($params);
		
		$base_url = $method == 'search' ? 'http://search.twitter.com/' : 'http://api.twitter.com/1/';
		
	    $opts = array(
			CURLOPT_RETURNTRANSFER => TRUE,
			CURLOPT_URL => $base_url.$method.'.json'.$query,
			CURLOPT_USERAGENT => self::$user_agent,
		);
		$ch = curl_init();
		curl_setopt_array($ch, $opts);
		$response = json_decode(curl_exec($ch), TRUE);
		curl_close($ch);

	    return $method == 'search' ? $response['results'] : $response;
	}
	
	public static function parse($tweet)
	{
		return self::parse_hashtags(self::parse_users(self::parse_links($tweet)));
	}
	
	public static function parse_links($tweet)
	{
		return preg_replace('@(https?://([-\w\.]+)+(/([\w/_\.]*(\?\S+)?(#\S+)?)?)?)@',
			'<a href="$1">$1</a>', $tweet);
	}
	
	public static function parse_users($tweet)
	{
		return preg_replace('/@(\w+)/',
		    '<a href="http://twitter.com/$1">@$1</a>', $tweet);
	}
	
	public static function parse_hashtags($tweet)
	{
		return preg_replace('/\s+#(\w+)/',
			' <a href="http://search.twitter.com/search?q=%23$1">#$1</a>', $tweet);
	}
	
}