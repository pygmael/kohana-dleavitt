<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Timestamped extends ORM {
	
	protected $_updated_column = array('column' => 'updated_at', 'format' => 'Y-m-d H:i:s');
	protected $_created_column = array('column' => 'created_at', 'format' => 'Y-m-d H:i:s');
	
	public function save()
	{
		if (is_array($this->_updated_column))
		{
			// Fill the updated column
			$column = $this->_updated_column['column'];
			$format = $this->_updated_column['format'];

			$data[$column] = $this->_object[$column] = ($format === TRUE) ? time() : date($format);
		}
		
		return parent::save();
	}
	
}
	