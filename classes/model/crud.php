<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_CRUD extends Model_Timestamped {
	
	public $errors = array();
	protected $_search_fields = array();
	
	public function search($query, $fields = array())
	{
		$search_fields = empty($fields) ? $this->_search_fields : $fields;
		foreach ($search_fields as $field)
		{
			$this->or_where($field, 'LIKE', '%'.$query.'%');
		}
		return $this;
	}
	
	public function check()
	{
		if ( ! parent::check())
		{
			$this->errors = $this->errors('validate');
			
			return FALSE;
		}
		return TRUE;
	}
	
	public function errors($file = NULL, $translate = TRUE)
	{
		return $this->_validate->errors($file, $translate);
	}
	
}


