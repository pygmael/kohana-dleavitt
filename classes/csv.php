<?php defined('SYSPATH') OR die('No direct access allowed.');

class CSV {

	public static function wrap_cell($cell)
	{
		return '"'.str_replace("\n", ' ', str_replace('"', '\'\'', $cell)).'"';
	}
	
	public static function wrap_row($row)
	{
		$string = array();
		foreach ($row as $cell)
		{
			$string[] = CSV::wrap_cell($cell);
		}
		return implode(',', $string);
	}
	
	public static function wrap_table($table)
	{
		$string = array();
		foreach ($table as $label => $row)
		{
			array_unshift($row, $label);
			$string[] = CSV::wrap_row($row);
		}
		return implode("\n", $string);
	}
	
}