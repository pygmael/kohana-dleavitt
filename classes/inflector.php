<?php defined('SYSPATH') or die('No direct access allowed.');

class Inflector extends Kohana_Inflector {
	
	public static function title($str)
	{
		return ucwords(Inflector::humanize($str));
	}
	
}