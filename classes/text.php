<?php defined('SYSPATH') OR die('No direct access allowed.');

class Text extends Kohana_Text
{
	public static function strip_nonalpha($str, $replace = '')
	{
 		return preg_replace("/[^a-zA-Z0-9\s]/", $replace, $str);
	}
}

