<?php defined('SYSPATH') or die('No direct script access.');

class Database_Query_Builder_Insert extends Kohana_Database_Query_Builder_Insert {
	
	public function value_array(array $values)
	{
		foreach ($values as $value) $this->values($value);
		
		return $this;
	}
	
}