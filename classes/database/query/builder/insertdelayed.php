<?php defined('SYSPATH') or die('No direct script access.');

class Database_Query_Builder_InsertDelayed extends Database_Query_Builder_Insert {
	
	public function compile(Database $db)
	{
		$query = parent::compile($db);
		$query = str_replace('INSERT INTO', 'INSERT DELAYED INTO', $query);
		return $query;
	}
	
}