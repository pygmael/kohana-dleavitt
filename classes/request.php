<?php defined('SYSPATH') or die('No direct script access.');

class Request extends Kohana_Request {
	
	public function set($key, $value)
	{
		$this->_params[$key] = $value;
		return $this;
	}
	
}