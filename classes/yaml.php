<?php defined('SYSPATH') OR die('No direct access allowed.');

require_once Kohana::find_file('vendor','spyc');

class YAML {
	
	public static function load($file)
	{
		return Spyc::YAMLLoad($file);
	}
	
	public static function load_string($string)
	{
		return Spyc::YAMLLoadString($string);
	}
	
	public static function dump($array, $indent = FALSE, $wordwrap = FALSE)
	{
		return Spyc::YAMLDump($array, $indent, $wordwrap);
	}
	
}