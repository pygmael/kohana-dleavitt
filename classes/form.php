<?php defined('SYSPATH') or die('No direct access allowed.');

class Form extends Kohana_Form {

	public static function input($name, $value = NULL, array $attributes = NULL)
	{
		if ($attributes === array())
		{
			$attributes = array('id' => $name);
		}
	
		return Kohana_Form::input($name, $value, $attributes);
	}
}