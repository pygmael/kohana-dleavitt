<?php defined('SYSPATH') or die('No direct script access.');

abstract class Controller_Template_Twig extends Kohana_Controller_Template_Twig 
{
	public $single_action = FALSE;
	
	public function before()
	{
		// Auto-generate template filename
		if (empty($this->template))
		{
			$this->template = $this->request->controller;
			
			if ( ! $this->single_action)
			{
				$this->template .= '/'.$this->request->action;
			}
			
			// Prepend directory if needed
			if (!empty($this->request->directory))
			{
				$this->template = $this->request->directory.'/'.$this->template;
			}

			// Convert underscores to slashes
			$this->template = str_replace('_', '/', $this->template);
		}
		
		if ($this->auto_render)
		{
			// Load the twig template.
			$this->template = Twig::factory($this->template, $this->environment);

			// Return the twig environment
			$this->environment = $this->template->environment();
		}
	}

} 
