<?php defined('SYSPATH') or die('No direct script access.');

abstract class Controller_API_Base extends Controller {
	
	public $status;
	public $data;
	public $message;
	
	public $require_login = FALSE;
	public $required_role = NULL;

	public function before()
	{
		parent::before();
		
		// TODO: should be able to pass token
		if ($this->require_login AND ! Auth::instance()->logged_in($this->required_role))
		{
			$this->set_error('Must be logged in.');
			$this->after();
			die($this->request->response);
		}
	}

	public function after()
	{	
		if (Arr::get($_REQUEST, 'debug'))
		{
			echo Kohana::debug(array(
				'status' => $this->status,
				'data' => $this->data,
				'message' => $this->message,
				));
			echo View::factory('profiler/stats');
		}
		else
		{
			$this->request->headers['Content-Type'] = 'application/json';
			$this->request->response = json_encode(array(
				'status' => $this->status,
				'data' => $this->data,
				'message' => $this->message,
			));
		}

		return parent::after();
	}

	public function set_success($data, $message = NULL)
	{
		$this->status = TRUE;
		$this->data = $data;
		if ($message !== NULL)
		{
			$this->message = $message;
		}
	}

	public function set_error($message, $data = NULL)
	{
//		$this->request->status = 400;
		$this->status = FALSE;
		$this->message = $message;
		if ($data !== NULL)
		{
			if ($data instanceof Exception)
				{
					$this->data = array(
						'message' => $data->getMessage(),
						'code' => $data->getCode(),
						'file' => $data->getFile(),
						'line' => $data->getLine(),
						'trace' => explode("\n", $data->getTraceAsString()),
		//				'trace' => $ex->getTrace(), // recursion
					);
				}
				else
				{
					$this->data = $data;
				}
			
		}
	}
}