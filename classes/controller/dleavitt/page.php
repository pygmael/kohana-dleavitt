<?php defined('SYSPATH') OR die('No direct access allowed.');

abstract class Controller_DLeavitt_Page extends Controller_Template_Twig {
	
	public $single_action = FALSE;
	
	public $controller_name;
	
	public $title = 'IdeaCorps';
	public $subtitle;
	
	public $body_class = 'main';
	
	public $messages = array();
	
	public $logged_in = FALSE;
	
	public function __construct($request)
	{
		if ($this->single_action)
		{
			$this->template = $request->controller;
			
			if (!empty($request->directory))
			{
				$this->template = $request->directory.'/'.$this->template;
			}
			
			$this->template = str_replace('_', '/', $this->template);
		}
		
		parent::__construct($request);
	}
	
	public function before()
	{	
		parent::before();
		
//		$this->messages = Message::get();
		$this->template
			->bind('request', $this->request)
			->bind('title', $this->title)
			->bind('subtitle', $this->subtitle)
			->bind('messages', $this->messages)
			->bind('styles', $this->styles)
			->bind('scripts', $this->scripts)
			->bind('body_class', $this->body_class);
		
		if (! IN_PRODUCTION AND isset($_GET['debug'])) 
		{
			$this->template->profiler = View::factory('profiler/stats');
			$this->template->sessions = View::factory('profiler/keyval')
				->set('name', 'Sessions')
				->set('data', $_SESSION);
		}
		

	}
	
}