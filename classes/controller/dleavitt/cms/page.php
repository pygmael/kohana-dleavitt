<?php defined('SYSPATH') OR die('No direct access allowed.');

abstract class Controller_DLeavitt_CMS_Page extends Controller_Page {
	
	public $body_class = 'cms';
	public $login_url = 'auth/login';
	public $require_login = TRUE;
	public $required_role = NULL;
	public $sections = array();
	
	public function before()
	{
		parent::before();
		
		Session::instance()->set('crumb', $this->request->uri);

		if ($this->require_login AND ! Auth::instance()->logged_in($this->required_role))
		{
			$this->request->redirect($this->login_url);
		}

		$this->template->sections = $this->sections;
		$this->template->user = Auth::instance()->get_user();
	}
	
}