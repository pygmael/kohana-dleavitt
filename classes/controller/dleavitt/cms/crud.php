<?php defined('SYSPATH') OR die('No direct access allowed.');

abstract class Controller_DLeavitt_CMS_CRUD extends Controller_CMS_Page {

	public $model_name;
	public $model;
	public $plural;
	public $singular;
	public $items_per_page = 0;

	public function __construct($request)
	{
		parent::__construct($request);
		
		if ( ! isset($this->model_name)) $this->model_name = Inflector::singular($this->request->controller);
		if ( ! isset($this->model)) $this->model = ORM::factory($this->model_name);
		if ( ! isset($this->plural)) $this->plural = Inflector::humanize($this->request->controller);
		if ( ! isset($this->singular)) $this->singular = ucwords(Inflector::singular($this->plural));
	}
	
	########################################
	##  ACTIONS
	########################################
	
	public function action_index()
	{
		$this->generate_list();
		$this->generate_headings();
	}
	
	public function action_delete($id)
	{		
		$this->model->find($id);
		
		//if ( ! $this->model->loaded()) Event::run('system.404');
		$this->model->delete();
		
		Message::add($this->model->{$this->model->primary_val()} . ' deleted.');
		$this->request->redirect('cms/'.$this->request->controller);
	}
	
	// view/add/update
	public function action_detail($id = NULL)
	{
		$this->template->bind('model', $this->model);
		
		if ($id !== NULL) $this->model->find($id);
		
		if ($_POST)
		{
			$this->model->values($_POST);
			
			if ($this->model->check())
			{
				$this->model->save();
				Message::add($this->model->{$this->model->primary_val()} . ' saved.');
				$this->request->redirect('cms/'.$this->request->controller);
			}
			else
			{
				$this->messages[] = new Message($this->model->{$this->model->primary_val()} . ' Form errors.', 'error');
			}
		}
	}
	
	########################################
	##  INDEX HELPER METHODS
	########################################
	
	protected function generate_headings()
	{
		$this->template->headings = Pygmael_Heading::generate_multiple($this->model->summary_labels());
	}
	
	protected function generate_list()
	{
		// search params
		if ($query = Arr::get($_GET, 'query')) 
		{
			if (! Arr::get($_GET, 'clear'))
			{
				$this->model->search($query);
				$this->template->query = $query;
			}
		}
		
		if ($order_by = Arr::get($_GET, 'order_by'))
		{
			$this->model->order_by($order_by, Arr::get($_GET, 'sort_direction', 'ASC'));
		}
		
		// paginate
		if ($this->items_per_page)
		{	
			$this->model->limit($this->items_per_page);
			$this->model->offset((Arr::get($_GET, 'page', 1) - 1) * $this->items_per_page);
			
			if ( ! isset($this->models)) $this->models = $this->model->find_all();
			$total_items = $this->model->count_last_query();
			
			if ($total_items > $this->items_per_page)
			{
				$pager = Pagination::factory(array(
					'total_items' => $total_items,
					'items_per_page' => $this->items_per_page,
				));
			}
		}
		
		if ( ! isset($this->models)) $this->models = $this->model->find_all();
		
		$this->template
			->bind('models', $this->models)
			->set('pager', (isset($pager) ? $pager : ''));
	}


}