<?php defined('SYSPATH') or die('No direct script access.');

class Date extends Kohana_Date {
	
	public static function span_string($time1 = NULL, $time2 = NULL, $output = 'years,months,weeks,days,hours,minutes,seconds')
	{
		
		if ($time1 == $time2)
		{
			return '0 seconds';
		}
		$span = Date::span($time1, $time2, $output);
		if (is_array($span)) 
		{
			$span = implode(' ', array_filter(array_map('Date::combine_key_val', $span, array_keys($span))));
		}
		
		return $span;
	}
	
	protected static function combine_key_val($value, $label)
	{
		if ($value) return $value.' '.$label;
	}
	
}