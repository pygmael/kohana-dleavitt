<?php defined('SYSPATH') or die('No direct access allowed.');

class URL extends Kohana_URL {
	
	public static function asset($file, $index = FALSE)
	{
		return (strpos($file, '://') === FALSE) ? URL::base($index).$file : $file;
	}
	
	public static function filepath_to_url($filepath)
	{
		return URL::site(str_replace(DOCROOT, '/', $filepath), TRUE);
	}
	
} // End url