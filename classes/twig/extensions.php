<?php defined('SYSPATH') or die('No direct script access.');


class Twig_Extensions extends Kohana_Twig_Extensions
{
	public function getTokenParsers()
	{
		return array(
			new Kohana_Twig_HTML_TokenParser(),
			new Kohana_Twig_Form_TokenParser(),
			new Twig_URL_TokenParser(),
			new Kohana_Twig_Cache_TokenParser(),
			new Kohana_Twig_Trans_TokenParser(),
		);
	}
	
	public function getFilters()
	{	
		return array_merge(parent::getFilters(), array(
			'max' => new Twig_Filter_Function('max'),
			'min' => new Twig_Filter_Function('min'),
			'underscore' => new Twig_Filter_Function('Inflector::underscore'),
			'strip_nonalpha' => new Twig_Filter_Function('Text::strip_nonalpha'),
			'debug' => new Twig_Filter_Function('Kohana::debug'),
            'date2'   => new Twig_Filter_Function('Twig_Extensions::date_r'),
		));
	}
	
	public static function date_r($timestamp, $format)
	{
		return date($format, $timestamp);
	}
}
