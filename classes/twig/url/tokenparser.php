<?php defined('SYSPATH') or die('No direct script access.');

class Twig_URL_TokenParser extends Kohana_Twig_Helper_TokenParser
{

	public function getTag()
	{
		return 'url';
	}
}
