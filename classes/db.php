<?php defined('SYSPATH') or die('No direct script access.');

class DB extends Kohana_DB {
	
	public static function replace($table, array $columns = NULL)
	{
		return new Database_Query_Builder_Replace($table, $columns);
	}
	
	public static function insert_delayed($table, array $columns = NULL)
	{
		return new Database_Query_Builder_InsertDelayed($table, $columns);
	}
	
}